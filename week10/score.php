<?php
session_start();
include 'question.php';
$score = 0;

$cookie_duration = strtotime('+1 days');

$all_correct_answers = array();
$all_quest = array_merge($questions_page_1, $questions_page_2);
foreach(array_merge($questions_page_1, $questions_page_2) as $question){
    $all_correct_answers["ques_".$question->number] = $question->get_correct();
}

if($_POST){
    foreach($_POST as $key => $value) {
        setcookie($key, $value, $cookie_duration);
        if (in_array($key, array_keys($all_correct_answers))) {
            if ($_POST[$key] == $all_correct_answers[$key]) {
                $score++;
            }
            unset($all_correct_answers[$key]);
        }
    }
};

foreach($_COOKIE as $key => $value) {
    if (in_array($key, array_keys($all_correct_answers))) {
        if ($_COOKIE[$key] == $all_correct_answers[$key]) {
            $score++;
        }
    }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" href="style.css" />
    <form action="score.php" method="post">
    <title>Quizz</title>
</head>
<body>
<div class="container score-board">
    <div class="h1 text-center text" >Your Score</div>
    <div class="h4 text-center score"><?php echo $score?> / 10</div>
    <div class='h6 text-center descript' >
        <?php
        
        if ($score < 4) {
            echo 'Gà quá !';
        } else if ($score >=4 && $score < 7){
            echo 'Thường thôi';
        } else {
            echo 'Cũng ghê đấy';
        }
        ?>
    </div>
    <?php
        foreach($all_quest as $ques){
            echo "
            <div class='container mt-sm-5 my-1'>
                <div class='question ml-sm-5 pl-sm-5 pt-2'>
                    <div class='py-2 h5'>
                        <b>Question $ques->number. $ques->question</b>
                    </div>
            ";
            echo "<div class='ml-md-3 ml-sm-3 pl-md-5 pt-sm-0 pt-3' id='options'>";
            for ($i=0 ; $i<=3 ; $i++){
                $q = $ques->answers[$i];
                $c = $ques->get_correct();
                $val = $i + 1;

                if (isset($_COOKIE['ques_'.$ques->number]) and $_COOKIE['ques_'.$ques->number] == $val) {
                    if ($val == $c) {
                        echo "<label class='options option-correct' style='color: #00d82b;'> $q";
                    }
                    else {
                        echo "<label class='options option-incorrect' style='color: red;'> $q";
                    }
                    echo "<input type='checkbox' name='ques_$ques->number' disabled='disabled' value=$val checked>";
                } else {
                    if ($val == $c) {
                        echo "<label class='options option-correct' style='color: #00d82b;'> $q
                                <input type='checkbox' name='ques_$ques->number' disabled='disabled' value=$val checked>
                        ";
                    }
                    else {
                        echo "<label class='options' >$q<input type='checkbox' name='ques_$ques->number' disabled='disabled' value=$val>";
                    }
                }
                echo "      <span class='checkmark'></span>
                        </label>
                ";
            }
            echo "
                    </div>
                </div>
            </div>
            ";
        }
        ?>

</div>

</body>
</html>

