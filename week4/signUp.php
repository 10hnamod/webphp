<!DOCTYPE html> 
<html lang='vn'> 
<head><meta charset='UTF-8'></head> 
<title>Login</title>
<body>

    <fieldset style='width: 450px; height: 400px; margin: auto; border:#ADD8E6 solid'>
    <?php $name = $gender = $industryCode = $birthOfDate = $address= "";

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if(empty(inputHandling($_POST["name"]))){
                echo "<div style='color: red;'>Hãy nhập tên</div>";
            }
            if (empty($_POST["gender"])) {
                echo "<div style='color: red;'>Hãy chọn giới tính</div>";
            }
            if(empty(inputHandling($_POST["industryCode"]))){
                echo "<div style='color: red;'>Hãy chọn phân khoa</div>";
            }

            if(empty(inputHandling($_POST["birthOfDate"]))){
                echo "<div style='color: red;'>Hãy nhập ngày sinh</div>";
            }
            elseif (!validateDate($_POST["birthOfDate"])) {
                echo "<div style='color: red;'>Hãy nhập ngày sinh đúng định dạng</div>";
            }
        }

        function inputHandling($data) {
            $data = trim($data);
            $data = stripslashes($data);
            return $data;
        }

        function validateDate($date){
            if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date)) {
                return true;
            } else {
                return false;
            }
        }
    ?>
    <form style='margin: auto' method="post">
        <table style = 'border-collapse:separate; border-spacing: 35px 15px;'>
            <tr height = '40px'>
                <td width = 10% style = 'background-color: #339900; vertical-align: center; text-align: center;'>
                    <label style='color: white;'>
                        Họ và tên
                        <span style = 'color: #FF0000'>
                             * 
                        </span>
                    </label>
                </td>
                <td width = 10% >
                    <input type='text' name = "name" style = 'width: 100%; line-height: 25px; border-color:#ADD8E6'>
                </td>
            </tr>
            <tr height = '40px'>
                <td width = 30% style = 'background-color: #339900; vertical-align: center; text-align: center;'>
                    <label style='color: white;'>Giới tính<span style = 'color: #FF0000'> * </span></label>
                </td>
                <td width = 30% >
                <?php $genderArr=array("Nam","Nữ");
                    for($x = 0; $x < count($genderArr); $x++){
                        echo"
                            <label class='container'>
                                <input type='radio' value=".$genderArr[$x]." name='gender'>"
                                .$genderArr[$x]. 
                            "</label>";
                    }
                ?>  
                </td>
            </tr>
            <tr height = '40px'>
                <td style = 'background-color: #339900; vertical-align: center; text-align: center;'>
                    <label style='color: white;'>Phân Khoa<span style = 'color: #FF0000'> * </span></label>
                </td>
                <td height = '40px'>
                    <select name='industryCode' style = 'border-color:#ADD8E6;height: 30px;width: 75%;'>
                        <?php
                            $industryCodeArr=array("EMPTY"=>"","MAT"=>"Khoa học máy tính","KDL"=>"Khoa học vật liệu");
                            foreach($industryCodeArr as $x=>$x_value){
                                echo"<option>".$x_value."</option>";
                            }
                        ?>
                    </select>
                </td>
            </tr>
            
            <tr height = '40px'>
                <td style = 'background-color: #339900; vertical-align: center; text-align: center;'>
                    <label style='color: white;'>
                        Ngày sinh
                        <span style = 'color: #FF0000'>
                        * 
                        </span></label>
                </td>
                <td height = '40px'>
                    <input type='date' name="birthOfDate" data-date="" data-date-format="DD MMMM YYYY" style = 'width: 73%; line-height: 25px; border-color:#ADD8E6; color: grey;'>
                </td>
            </tr>

            <tr height = '40px'>
                <td style = 'background-color: #339900; vertical-align: center; text-align: center;'>
                    <label style='color: white;'>
                        Địa chỉ
                    </label>
                </td>
                <td height = '40px'>
                    <input type='text' name="address" style = 'width: 100%; line-height: 25px; border-color:#ADD8E6'> 
                </td>
            </tr>
            
        </table>
        <button style='background-color: #339900; border-radius: 10px; width: 25%; height: 43px; border-width: 0; margin: 20px 175px; color: white;'>
            Đăng Kí
        </button>
    </form>

</fieldset>
</body>
</html>