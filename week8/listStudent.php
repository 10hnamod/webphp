<?php
$spec = array("MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
?>

<!DOCTYPE html>
<html lang="en">

<?php
    session_start();
    $faculty_code = '';
    $key_word = '';

    if(isset($_SESSION["faculty_code"])&&isset($_SESSION["key_word"])){
        $faculty_code = $_SESSION["faculty_code"];
        $key_word = $_SESSION["key_word"];
    }

    if(isset($_POST['search'])){
        if(isset($_POST['faculty_code'])&&isset($_POST['key_word'])) {
            $faculty_code = $_POST['faculty_code'];
            $key_word = $_POST['key_word'];
            $_SESSION["faculty_code"] = $_POST['faculty_code'];
            $_SESSION["key_word"] = $_POST['key_word'];
        }
    } elseif (isset($_POST['delete'])){
        $faculty_code = '';
        $key_word = '';
        $_SESSION["faculty_code"] = '';
        $_SESSION["key_word"] = '';
    }

?>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>List student</title>
    <style>
    body {
        font-family: sans-serif;
        padding: 10px;
    }

    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }
    .container {
        margin: 0 25%;
        border: 1px solid black;
    }

    .search-form {
        display: flex;
        flex-direction: column;
        width: 40%;
        padding: 10px 30px;
    }

    .form-item p {
        padding: 12px 10px 5px 10px;
        color: black;
    }

    input[type="text"] {
        width: 100%;
        border: solid 2px #007bc7;
        outline: none;
        padding: 8px;
    }

    select {
        border: solid 2px #007bc7;
        outline: none;
        width: 100%;
        padding: 8px;
    }

    input[type="submit"] {
        padding: 12px 30px;
        background-color: #3984bc;
        border-radius: 10px;
        border: solid 2px #007bc7;
        cursor: pointer;
        color: white;
        font-size: 17px;
    }

    table {
        width: 100%;
        border-collapse: separate;
        border-spacing: 0 1em;
        padding: 0 20px;
    }

    td,
    th {
        text-align: left;
        padding: 6px;
    }

    form td {
        text-align: center;
    }
     .result-search {
    display: flex;
    justify-content: space-between;
    padding: 0 20px;
    }
    
    .action {
        color: white;
        display: inline-block;
        padding: 10px;
        background-color: #3984bc;
        border: 1px solid #007bc7;
        text-decoration: none;
        border-radius: 10px;
        margin-right: 8px;
    }
    </style>
</head>

<body>
    <div class="container">
    <div class="search-form">
        <form name="registerForm" method="POST" enctype="multipart/form-data" action="">
            <table>
                <tr class="form-item">
                    <td>
                        <p>
                            <label for="spec">
                                Khoa
                            </label>
                        </p>
                    </td>
                    <td>
                        <select name="faculty_code" id="spec">
                            <?php
                                if($faculty_code==''){
                                    $industryCodeArr=array("EMPTY"=>"","MAT"=>"Khoa học máy tính","KDL"=>"Khoa học vật liệu");
                                    foreach($industryCodeArr as $x=>$x_value){
                                        echo"<option>".$x_value."</option>";
                                    }
                                }else{
                                    echo "<option>".$faculty_code."</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="form-item">
                        <p>
                            <label for="spec">
                                Từ khóa
                            </label>
                        </p>
                    </td>
                    <td>
                        <input type="text" name="key_word" value="<?php echo $key_word; ?>">
                    </td>
                </tr>
                <tr>
                    <td><input type="submit" name="delete" value="Xóa"></td>
                    <td><input type="submit" name="search" value="Tìm kiếm"></td>
                </tr>
            </table>
        </form>
    </div>
    <div class="result-search">
        <div class="text">
        <span>Số sinh viên tìm thấy: XXX</span>
          </div>
        <div class="wrap-action">
            <a class="action" href="./register.php">Thêm</a>
        </div>
    </div>
        <div class="table-student">
            <table>
                <colgroup>
                    <col span="1" style="width: 10%;">
                    <col span="1" style="width: 25%;">
                    <col span="1" style="width: 50%;">
                    <col span="1" style="width: 15%;">
                </colgroup>
                <tr>
                    <th>No</th>
                    <th>Tên sinh viên</th>
                    <th>Khoa</th>
                    <th>Action</th>
                </tr>
                <tr>
                    <td>1</td>
                    <td>Nguyễn Văn A</td>
                    <td>Khoa học máy tính</td>
                    <td><a class="action" href="">Xóa</a></td>
                    <td><a class="action" href="">Sửa</a></td>  
                </tr>
                <tr>
                    <td>2</td>
                    <td>Trần Thị B</td>
                    <td>Khoa học máy tính</td>
                    <td><a class="action" href="">Xóa</a></td>
                    <td><a class="action" href="">Sửa</a></td>  
                </tr>
                <tr>
                    <td>3</td>
                    <td>Nguyễn Hoàng C</td>
                    <td>Khoa học vật liệu</td>
                    <td><a class="action" href="">Xóa</a></td>
                    <td><a class="action" href="">Sửa</a></td>  
                </tr>
                <tr>
                    <td>4</td>
                    <td>Đinh Quang D</td>
                    <td>Khoa học vật liệu</td>
                    <td><a class="action" href="">Xóa</a></td>
                    <td><a class="action" href="">Sửa</a></td>  
                </tr>
            </table>
        </div>
    </div>
    </div>
</body>

</html>
