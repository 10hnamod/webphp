<?php
class Question {
    public $number;
    public $question;
    public $answers;
    private $correct_answer;

    public function __construct($number, $question, $answers, $correct_answer){
        $this->number = $number;
        $this->question = $question;
        $this->answers = $answers;
        $this->correct_answer = $correct_answer;
    }

    public function get_correct(){
        return $this->correct_answer;
    }
}

$questions_page_1 = array(
    new Question(1, "Chương trình dịch là:", array("A. Chương trình có chức năng chuyển đổi chương trình được viết bằng ngôn ngữ lập trình bậc cao thành chương trình thực hiện được trên máy tính cụ thể.", "B. Chương trình có chức năng chuyển đổi chương trình được viết bằng ngôn ngữ lập trình bậc thấp thành ngôn ngữ bậc cao.", " C. Chương trình có chức năng chuyển đổi chương trình được viết bằng ngôn ngữ ngôn ngữ máy sang ngôn ngữ lập trình cụ thể.", "D. Chương trình có chức năng chuyển đổi chương trình được viết bằng ngôn ngữ máy sang hợp ngữ."), 1),
    new Question(2, "Biên dịch là:", array("A. Chương trình dịch, dịch toàn bộ chương trình nguồn thành một chương trình đích có thể thực hiện trên máy, không thể lưu trữ để sử dụng lại khi cần thiết.", "B. Chương trình dịch, dịch toàn bộ chương trình nguồn thành một chương trình đích có thể thực hiện trên máy và có thể lưu trữ để sử dụng lại khi cần thiết.", "C. Chương trình dịch, dịch toàn bộ ngôn ngữ lập trình bậc thấp sang ngôn ngữ lập trình bậc cao. ", "D. Chương trình dịch, lần lượt dịch và thực hiện từng câu lệnh."), 2),
    new Question(3, "Thông dịch là:", array("A. Chương trình dịch, dịch toàn bộ chương trình nguồn thành một chương trình đích có thể thực hiện trên máy, không thể lưu trữ để sử dụng lại khi cần thiết.", "B. Chương trình dịch, dịch toàn bộ chương trình nguồn thành một chương trình đích có thể thực hiện trên máy và có thể lưu trữ để sử dụng lại khi cần thiết.", "C. Chương trình dịch, dịch toàn bộ ngôn ngữ lập trình bậc thấp sang ngôn ngữ lập trình bậc cao. ", "D. Chương trình dịch, lần lượt dịch và thực hiện từng câu lệnh."), 4),
    new Question(4, "Nước không giáp với Việt Nam là:", array("Lào", "Cam-pu-chia", "Thái Lan", "Trung Quốc"), 3),
    new Question(5, "Sự giống nhau giữa thông dịch và biên dịch là:", array("A. Không phải chương trình dịch.", "B. Đều là chương trình dịch.", "C. Đều dịch từ ngôn ngữ lập trình bậc thấp sang ngôn ngữ lập trình bậc cao.", "D. Đều dịch từ ngôn ngữ máy sang hợp ngữ."), 2),
);


$questions_page_2 = array(
    new Question(6, "CLB vô địch Champions League/Cúp C1 châu Âu nhiều nhất trong lịch sử?", array("AC Milan", "Liverpool", "Real Madrid", "Manchester United"), 3),
    new Question(7, "Đất nước nào rộng thứ 2 thế giới ?", array("Nga", "Trung Quốc", "Mỹ", "Canada"), 4),
    new Question(8, "Đất nước nào đông dân thứ 2 thế giới ?", array("Trung Quốc", "Ấn Độ", "Mỹ", "Canada"), 2),
    new Question(9, "Đất nước nào bé thứ 2 thế giới ?", array("	Thành phố Vatican", "Monaco", "	Nauru", "Việt Nam"), 1),
    new Question(10, "Which among the following is Inner planet ?", array("Mercury", "Jupiter", "Venus", "Saturn"), 3),
);

$all_correct_answers = array();
foreach(array_merge($questions_page_1, $questions_page_2) as $question){
    $all_correct_answers["ques_".$question->number] = $question->get_correct();
}
?>

